/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const cardispatch = {
  path: '/cardispatch',
  component: Layout,
  redirect: 'noRedirect',
  name: 'cardispatch',
  meta: {
    title: '车辆管理',
    icon: 'component'
  },
  children: [
    {
      path: 'dispatchapplication',
      component: () => import('@/views/cardispatch/dispatchapplication'),
      name: 'dispatchapplication',
      meta: { title: '用车申请' }
    },
    {
      path: 'dispatchdepartment',
      component: () => import('@/views/cardispatch/dispatchdepartment'),
      name: 'dispatchdepartment',
      meta: { title: '部门审批' }
    },
    {
      path: 'dispatchlead',
      component: () => import('@/views/cardispatch/dispatchlead'),
      name: 'dispatchlead',
      meta: { title: '领导审批' }
    },
    {
      path: 'dispatchcarmovement',
      component: () => import('@/views/cardispatch/dispatchcarmovement'),
      name: 'dispatchcarmovement',
      meta: { title: '车辆调度' }
    },
    {
      path: 'dispatchcharge',
      component: () => import('@/views/cardispatch/dispatchcharge'),
      name: 'dispatchcharge',
      meta: { title: '接待办审批' }
    },
    {
      path: 'dispatchdata',
      component: () => import('@/views/cardispatch/dispatchdata'),
      name: 'dispatchdata',
      meta: { title: '车辆运行数据' }
    },
    {
      path: 'dispatchreturn',
      component: () => import('@/views/cardispatch/dispatchreturn'),
      name: 'dispatchreturn',
      meta: { title: '回场登记' }
    },
    {
      path: 'dispatproduct',
      component: () => import('@/views/cardispatch/dispatproduct'),
      name: 'dispatproduct',
      meta: { title: '生产运行科审批' }
    }
  ]
}

export default cardispatch
