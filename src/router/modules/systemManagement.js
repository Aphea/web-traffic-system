/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const systemManagementRouter = {
  path: '/systemManagement',
  component: Layout,
  redirect: 'noRedirect',
  name: 'SystemManagement',
  meta: {
    title: '系统管理',
    icon: 'component'
  },
  children: [
    {
      path: 'dataAuthorityManagement',
      component: () => import('@/views/systemManagement/dataAuthorityManagement'),
      name: 'DataAuthorityManagement',
      meta: { title: '数据权限管理' }
    },
    {
      path: 'modulePermissionManagement',
      component: () => import('@/views/systemManagement/modulePermissionManagement'),
      name: 'ModulePermissionManagement',
      meta: { title: '模块权限管理' }
    },
    {
      path: 'operationLogManagement',
      component: () => import('@/views/systemManagement/operationLogManagement'),
      name: 'OperationLogManagement',
      meta: { title: '操作日志管理' }
    },
    {
      path: 'propertyManagement',
      component: () => import('@/views/systemManagement/propertyManagement'),
      name: 'PropertyManagement',
      meta: { title: '属性管理' }
    },
    {
      path: 'roleInformationManagement',
      component: () => import('@/views/systemManagement/roleInformationManagement'),
      name: 'RoleInformationManagement',
      meta: { title: '角色信息管理' }
    },
    {
      path: 'userRoleManagement',
      component: () => import('@/views/systemManagement/userRoleManagement'),
      name: 'UserRoleManagement',
      meta: { title: '用户角色管理' }
    }
  ]
}

export default systemManagementRouter
